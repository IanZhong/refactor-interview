package main

import (
	"flag"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"zerologix.com/interview/routing"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
)

// change current working folder
func changeAppWorkingPath() {
	if exe, err := os.Executable(); err != nil {
		panic("SYS ERROR - get exe dir failed")
	} else {
		exPath := filepath.Dir(exe)
		if err := os.Chdir(exPath); err != nil {
			panic("SYS ERROR - changed working folder failed")
		}
	}
}

func configFile2Env() {
	var envfile string

	flag.StringVar(&envfile, "envfile", "config/.env", "environment config file")
	flag.Parse()

	if err := godotenv.Load(envfile); nil != err {
		panic("loading envfile failed: " + envfile)
	} else {
		log.Infof("Initial evnironment file;%s", envfile)
	}
}

func startLogger() {
	var logfile string

	flag.StringVar(&logfile, "logfile", "config/.log.yaml", "log config file")
	flag.Parse()
	log.Infof("initial log config file;%s", logfile)
}

// To check API response time
func timeCheckHandler(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var err error
		beforeTime := time.Now().UnixMilli()
		err = next(c)
		if err != nil {
			return err
		}
		url := c.Request().URL

		log.Debugf("%s response time is %d ms", url, time.Now().UnixMilli()-beforeTime)
		return nil
	}
}

func loadEcho() *echo.Echo {
	e := echo.New()
	e.HideBanner = true
	e.Use(middleware.Recover())
	return e
}

func bindSomethingWithEcho(e *echo.Echo) {
	// Read routing info from routing.yaml
	_, err := routing.Initialize(e, "config/routing.yaml")
	if err != nil {
		log.Fatalf("failed to initialize routing rules, reason;%s", err.Error())
	}
}

// server startup
// os.Getenv read environment variables will be merged into config file variables.
func startServer(e *echo.Echo) {
	host := os.Getenv("HOST")
	port := os.Getenv("RESTFUL_API_PORT")
	log.Infof("ltapi-gateway server startup on port;%s ...", port)
	if err := e.Start(host + ":" + port); nil != err && err != http.ErrServerClosed {
		log.Fatalf("ltapi-gateway server startup failed, reason; %s", err.Error())
	}
}

func main() {
	// set application working root path
	// so that we can load configure file from application location
	changeAppWorkingPath()

	startLogger()
	configFile2Env()

	// load Echo framework
	e := loadEcho()

	// load API Duration Check
	e.Use(timeCheckHandler)

	// mount routes controller
	bindSomethingWithEcho(e)

	// start the API Gateway
	go startServer(e)

	defer func() {
		log.Infof("ltaccess-quoting server closed")
	}()

	log.Infof("logixintelligence ltaccess-quoting is running")

	// waiting for interrupt me
	acyInterrupt := make(chan os.Signal, 1)
	signal.Notify(acyInterrupt, os.Interrupt, syscall.SIGTERM)
	existingNow := make(chan bool, 1)
	go func() {
		for {
			sig := <-acyInterrupt
			log.Infof("got interrupt signal;%v", sig)
			existingNow <- true
		}
	}()
	<-existingNow

	log.Infof("logixintelligence ltaccess-quoting exit now")
}
