# System Architecture
![Logo](./src/archi.png)

# API Invoke Flowchart For Creating Accounts
![Logo](./src/flow.png)

# API Gateway
- The API gateway serves as the central access point that routes requests to various services such as account, symbol, trading, quoting, and reporting services.

# Process Flow
The process begins with the creation of a trader and involves several steps:
- Create Trader: 
> A trader is created using the POST /ltaccount/v1/admin/traders endpoint.
- Extract Trader Information:
> The necessary information about the trader
> is extracted through the GET /ltreport/v1/admin/accounts 
> and GET /ltaccount/v1/admin/traders endpoints.
- Bind Trader to Symbol Group:
> The system retrieves existing symbol group associations for accounts
> using GET /ltsymbols/v1/admin/accountSymbolGroup. 
> A trader is then bind to a symbol group using POST /ltsymbols/v1/admin/accountSymbolGroup.
- Create Wallet:
> A wallet is created for the trader to facilitate transactions.
- Notify Quoting Server:
> The quoting server is notified about the new subscriber for ticks
> using POST /ltquoting/v1/admin/accountSymbolGroup.
- Extract Binding Result:
> The result of the symbol group binding is extracted
> using GET /ltsymbols/v1/admin/accountSymbolGroup.
- Final Notification:
> The trader is notified that his/her account has been 
> created and all setups are complete via GET /ltaccount/v1/admin/traders/sendemail.

# Task
In the provided flowchart detailing the trader account creation and management process, 
you've observed how various services interact through the API gateway to perform operations 
like creating a trader, binding to a symbol group, and notifying relevant services. 
Assume you are reviewing the code in `/controller/account_admin_create.go` which handles some of these steps.

1. Describe how you would review and potentially refactor the existing implementation in account_admin_create.go to improve its efficiency and maintainability. Consider aspects like error handling, API call optimization, and interaction with other services.
2. Given the complexity of interactions among services (e.g., account, symbol, trading), suggest an approach to streamline these interactions within the controller. Would you recommend any patterns or practices to ensure robustness and fault tolerance in the process?
3. Discuss how you would implement logging in this controller to effectively track the flow and diagnose issues. What key events would you log, and how would you ensure that the logs are useful for debugging while not overwhelming the log system?
4. Consider the error handling strategy within this controller. Based on the sequence of API calls and their dependencies as shown in the flowchart, how would you ensure that the system gracefully handles failures at different stages of the trader account setup? Provide examples of potential issues and how your implementation would address them.

# Assumption
1. All microservices are deployed in different EC2 instances and have their own Databases.