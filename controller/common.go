package controller

import (
	"github.com/labstack/echo/v4"
)

var ruleConfig RuleConfig

type RuleConfig interface {
	GetDomainAndMTLS(key string) (string, bool)
}

func GetDomainAndMTLS(ctx echo.Context, key string) (domain string, mTLS bool) {
	return ruleConfig.GetDomainAndMTLS(key)
}
