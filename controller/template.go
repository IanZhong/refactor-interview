package controller

import (
	"bytes"
	"io"
	"net/http"
	"strconv"
	"strings"

	"github.com/labstack/gommon/log"
)

type rewriteLogicHandler func(body []byte) ([]byte, error)

type ResponseWriterTemplate struct {
	callPath string
}

func (r *ResponseWriterTemplate) rewriteResTemplate(res *http.Response, handler rewriteLogicHandler) error {
	contentType := res.Header.Get("Content-Type")
	if strings.Contains(contentType, "text/csv") {
		// If the response is in CSV format, skip processing and return
		return nil
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Errorf("get response from %s failed, reason;%s", r.callPath, err.Error())
		return err
	}
	defer res.Body.Close()

	newBody, err := handler(body)

	if err != nil {
		return err
	}
	res.Body = io.NopCloser(bytes.NewReader(newBody))
	res.Header.Set("Content-Length", strconv.Itoa(len(newBody)))
	return nil
}
