package controller

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"zerologix.com/interview/ltconst"
	"zerologix.com/interview/lterror"
	"zerologix.com/interview/models"
	"zerologix.com/interview/service/httpclient"
)

type SymbolGroupController struct {
	Ctx     echo.Context
	Client  httpclient.IHttpClient
	ReqBody []byte
	Err     *models.CustomError
}

func (a *SymbolGroupController) RewriteResponse(res *http.Response) error {
	path := a.Ctx.Path()
	method := a.Ctx.Request().Method
	if method == ltconst.RounterActionGet {
		return nil
	}
	id := a.Ctx.Param("id")
	template := ResponseWriterTemplate{callPath: path}
	switch path {
	case "/ltsymbols/v1/admin/groups/:id":
		fallthrough
	case "/ltsymbols/v1/admin/group":
		fallthrough
	case "/ltsymbols/v1/admin/group/:id":
		fallthrough
	case "/ltsymbols/v1/admin/groups":
		if err := template.rewriteResTemplate(res, func(body []byte) ([]byte, error) {
			if res.StatusCode != 200 {
				log.Errorf("incorrect groups response status code;%d", res.StatusCode)
				return body, nil
			}

			responseBody := []map[string]interface{}{}
			if len(id) == 0 {
				var originalBody map[string]interface{}
				if err := json.Unmarshal(body, &originalBody); err != nil {
					log.Errorf("unmarshal response body failed, reason;%s", err.Error())
					return body, err
				}
				responseBody = append(responseBody, map[string]interface{}{"group_id": fmt.Sprintf("%v", originalBody["group_id"]), "action": ltconst.ActionAdd})
			} else {
				action := ""
				if method == ltconst.RounterActionPut {
					// get the original request
					var originalBody map[string]interface{}
					if err := json.Unmarshal(a.ReqBody, &originalBody); err != nil {
						log.Errorf("unmarshal request body failed, reason;%s", err.Error())
						return body, err
					}
					conditions, _ := originalBody["conditions"].(map[string]interface{})
					enableTrailingStops := conditions["enable_trailing_stops"].(string)

					// set action to ActionDisableTSL when disabling TSL
					action = ltconst.ActionUpdate
					if enableTrailingStops == "0" {
						action = ltconst.ActionDisableTSL
					}
				} else if method == ltconst.RounterActionDelete {
					action = ltconst.ActionDelete
				}
				responseBody = append(responseBody, map[string]interface{}{"group_id": id, "action": action})
			}

			modifiedBodyByte, err := json.Marshal(responseBody)
			if err != nil {
				log.Errorf("marshal modified body failed, reason;%s", err.Error())
				return body, err
			}

			// notify lttrading symbols changed
			var tradersRes *http.Response
			reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, len(modifiedBodyByte))
			tradingDomain, tradingMTLS := GetDomainAndMTLS(a.Ctx, "/lttrading/*")
			tradingReqUrl := fmt.Sprintf("%s/lttrading/v1/admin/group", tradingDomain)

			tradersRes, err = a.Client.Post(tradingReqUrl, reqHeaders, modifiedBodyByte, tradingMTLS)
			if err != nil {
				failedGroupId := responseBody[0]["group_id"]
				log.Errorf("notify lttrading failed, group_id; %s, reason;%s", failedGroupId, err.Error())
			} else {
				if tradersRes.StatusCode != 200 {
					log.Errorf("incorrect traders info response status code;%d", tradersRes.StatusCode)
				} else {
					log.Infof("notify lttrading groups changed successfully")
				}
			}

			// notify ltquoting group changed
			var quotingRes *http.Response
			quotingDomain, quotingMTLS := GetDomainAndMTLS(a.Ctx, "/ltquoting/*")
			quotingReqUrl := fmt.Sprintf("%s/ltquoting/v1/admin/group", quotingDomain)

			quotingRes, err = a.Client.Post(quotingReqUrl, reqHeaders, modifiedBodyByte, quotingMTLS)
			if err != nil {
				failedGroupId := responseBody[0]["group_id"]
				log.Errorf("notify ltquoting failed, group_id; %s, reason;%s", failedGroupId, err.Error())
			} else {
				if quotingRes.StatusCode != 200 {
					log.Errorf("incorrect ltquoting response status code;%d", quotingRes.StatusCode)
				} else {
					log.Infof("notify ltquoting groups changed successfully")
				}
			}

			// notify ltfix-gateway symbols groups changed
			// TBD: Currently ltfix-gateway don't need the group/symbol_group info except for the bid/ask markup and spreads.
			// The quoting might need the markup and spread to calculate the tick data for each individual trader. If that is
			// the case, the FE would need to pass the account_id to the ltquoting while subscribing the symbol quoting data
			// so that ltquoting can calculate the bid/ask according to the group of the trader.

			return body, nil

		}); err != nil {
			log.Debugf("rewrite response failed, reason;%s", err.Error())
			return err
		}
	case "/ltsymbols/v1/admin/groupSymbols/:id":
		fallthrough
	case "/ltsymbols/v1/admin/groupSymbol":
		fallthrough
	case "/ltsymbols/v1/admin/groupSymbol/:id":
		fallthrough
	case "/ltsymbols/v1/admin/groupSymbols":
		if err := template.rewriteResTemplate(res, func(body []byte) ([]byte, error) {
			if res.StatusCode != 200 {
				log.Errorf("incorrect groupSymbol response status code;%d", res.StatusCode)
				return body, nil
			}

			// for post to create symbol group under specific group id
			// TBD: it's a workaround, we need to enhance the following section. Now we just re-init symbol group when adding symbol group into specific group
			if len(id) > 0 && method == ltconst.RounterActionPost {
				tradingDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/lttrading/*")
				reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, len(body))
				requestUrl := fmt.Sprintf("%s/lttrading/v1/admin/group", tradingDomain)

				reqBody, err := json.Marshal([]map[string]interface{}{{
					"group_id": id,
					"action":   ltconst.ActionAllUpdate,
				}})
				if err != nil {
					log.Errorf("marshal request body failed, reason;%s", err.Error())
					return body, err
				}

				_, err = a.Client.Post(requestUrl, reqHeaders, reqBody, mTLS)
				if err != nil {
					log.Errorf("send request to lttrading failed, reason;%s", err.Error())
				} else {
					log.Infof("notify lttrading groupSymbol updated successfully")
				}

				// notify ltquoting that symbols group has changed
				var quotingRes *http.Response
				quotingDomain, quotingMTLS := GetDomainAndMTLS(a.Ctx, "/ltquoting/*")
				quotingReqUrl := fmt.Sprintf("%s/ltquoting/v1/admin/group", quotingDomain)

				quotingRes, err = a.Client.Post(quotingReqUrl, reqHeaders, reqBody, quotingMTLS)
				if err != nil {
					log.Errorf("notify ltquoting failed, group_id; %s, reason;%s", id, err.Error())
				} else {
					if quotingRes.StatusCode != 200 {
						log.Errorf("incorrect ltquoting response status code;%d", quotingRes.StatusCode)
					} else {
						log.Infof("notify ltquoting group changed successfully")
					}
				}
				return body, nil
			}

			responseBody := []map[string]interface{}{}
			if len(id) == 0 {
				return body, errors.New("group id can not be empty")
			} else {
				action := ""
				if method == ltconst.RounterActionPut {
					action = ltconst.ActionUpdate
				} else if method == ltconst.RounterActionDelete {
					action = ltconst.ActionDelete
				}
				responseBody = append(responseBody, map[string]interface{}{"group_symbol_id": id, "action": action})
			}

			modifiedBodyByte, err := json.Marshal(responseBody)
			if err != nil {
				log.Errorf("marshal modified body failed, reason;%s", err.Error())
				return body, err
			}

			// notify lttrading symbols groups changed
			var tradersRes *http.Response
			reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, len(modifiedBodyByte))

			tradingDomain, tradingMTLS := GetDomainAndMTLS(a.Ctx, "/lttrading/*")
			tradingReqUrl := fmt.Sprintf("%s/lttrading/v1/admin/groupSymbol", tradingDomain)

			tradersRes, err = a.Client.Post(tradingReqUrl, reqHeaders, modifiedBodyByte, tradingMTLS)
			if err != nil {
				failedGroupSymId := responseBody[0]["group_symbol_id"]
				log.Errorf("notify lttrading failed, group_symbol_id; %s, reason;%s", failedGroupSymId, err.Error())
			} else {
				if tradersRes.StatusCode != 200 {
					log.Errorf("incorrect traders info response status code;%d", tradersRes.StatusCode)
				} else {
					log.Infof("notify lttrading groupSymbol changed successfully")
				}
			}

			// notify ltquoting that symbols groups has changed
			var quotingRes *http.Response
			quotingDomain, quotingMTLS := GetDomainAndMTLS(a.Ctx, "/ltquoting/*")
			quotingReqUrl := fmt.Sprintf("%s/ltquoting/v1/admin/groupSymbol", quotingDomain)

			quotingRes, err = a.Client.Post(quotingReqUrl, reqHeaders, modifiedBodyByte, quotingMTLS)
			if err != nil {
				failedGroupSymId := responseBody[0]["group_symbol_id"]
				log.Errorf("notify ltquoting failed, group_symbol_id; %s, reason;%s", failedGroupSymId, err.Error())
			} else {
				if quotingRes.StatusCode != 200 {
					log.Errorf("incorrect ltquoting response status code;%d", quotingRes.StatusCode)
				} else {
					log.Infof("notify ltquoting groupSymbol changed successfully")
				}
			}

			// notify ltfix-gateway symbol group added
			// TBD: Currently ltfix-gateway don't need the group/symbol_group info except for the bid/ask markup and spreads.
			// The quoting might need the markup and spread to calculate the tick data for each individual trader. If that is
			// the case, the FE would need to pass the account_id to the ltquoting while subscribing the symbol quoting data
			// so that ltquoting can calculate the bid/ask according to the group of the trader.

			return body, nil

		}); err != nil {
			log.Debugf("rewrite response failed, reason;%s", err.Error())
			return err
		}
	case "/ltsymbols/v1/admin/accountSymbolGroup":
		if err := template.rewriteResTemplate(res, func(body []byte) ([]byte, error) {
			if res.StatusCode != 200 {
				log.Errorf("incorrect account symbol group response status code;%d", res.StatusCode)
				return body, nil
			}

			responseBody := []map[string]interface{}{}
			var originalBody map[string]interface{}
			if err := json.Unmarshal(a.ReqBody, &originalBody); err != nil {
				log.Errorf("unmarshal request body failed, reason;%s", err.Error())
				return body, err
			}
			responseBody = append(responseBody, map[string]interface{}{"group_id": fmt.Sprintf("%v", originalBody["symbol_group_id"]), "account_number": fmt.Sprintf("%v", originalBody["account_number"]), "action": ltconst.ActionAdd})

			modifiedBodyByte, err := json.Marshal(responseBody)
			if err != nil {
				log.Errorf("marshal modified body failed, reason;%s", err.Error())
				return body, err
			}

			var tradersRes *http.Response
			reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, len(modifiedBodyByte))

			tradingDomain, tradingMTLS := GetDomainAndMTLS(a.Ctx, "/lttrading/*")
			tradingReqUrl := fmt.Sprintf("%s/lttrading/v1/admin/accountSymbolGroup", tradingDomain)

			tradersRes, err = a.Client.Post(tradingReqUrl, reqHeaders, modifiedBodyByte, tradingMTLS)
			if err != nil {
				failedAccId := responseBody[0]["account_number"]
				failedGroupId := responseBody[0]["group_id"]
				failedAction := responseBody[0]["action"]
				log.Errorf("notify lttrading failed, account_number; %v, symbol_group_id; %v, action; %v, reason;%s", failedAccId, failedGroupId, failedAction, err.Error())
			} else {
				s, err := io.ReadAll(tradersRes.Body)
				if err != nil || tradersRes.StatusCode != 200 {
					log.Errorf("incorrect traders info response, status_code;%d, resp;%s, reason;%v", tradersRes.StatusCode, string(s), err)
				} else {
					log.Infof("notify lttrading account symbol group successfully, resp;%s", string(s))
				}
			}

			// notify ltquoting account symbol group changed
			var quotingRes *http.Response
			quotingDomain, quotingMTLS := GetDomainAndMTLS(a.Ctx, "/ltquoting/*")
			quotingReqUrl := fmt.Sprintf("%s/ltquoting/v1/admin/accountSymbolGroup", quotingDomain)

			quotingRes, err = a.Client.Post(quotingReqUrl, reqHeaders, modifiedBodyByte, quotingMTLS)
			if err != nil {
				failedAccId := responseBody[0]["account_number"]
				failedGroupId := responseBody[0]["group_id"]
				failedAction := responseBody[0]["action"]
				log.Errorf("notify ltquoting failed, account_number; %v, symbol_group_id; %v, action; %v, reason;%s", failedAccId, failedGroupId, failedAction, err.Error())
			} else {
				if quotingRes.StatusCode != 200 {
					log.Errorf("incorrect ltquoting response status code;%d", quotingRes.StatusCode)
				} else {
					log.Infof("notify ltquoting account symbol group successfully")
				}
			}

			//TBD
			// notify ltfix-gateway account symbols group changed

			return body, nil

		}); err != nil {
			log.Debugf("rewrite response failed, reason;%s", err.Error())
			return err
		}
	}
	return nil
}

func (a *SymbolGroupController) assembleValidHeaders(echoReqHeaders map[string][]string, contentLength int) map[string]string {
	validHeaders := make(map[string]string)
	for key, value := range echoReqHeaders {
		if key != "Content-Length" {
			validHeaders[key] = value[0]
		}
	}
	validHeaders["Content-Length"] = strconv.Itoa(contentLength)
	validHeaders["Content-Type"] = "application/json"
	return validHeaders
}
func (a *SymbolGroupController) InjectCtx(ctx echo.Context) {
	a.Ctx = ctx
	path := a.Ctx.Path()
	method := a.Ctx.Request().Method
	var params map[string]string
	switch path {
	case "/ltsymbols/v1/admin/accountSymbolGroup":
		req := a.Ctx.Request()
		if method != ltconst.RounterActionPut && method != ltconst.RounterActionPost {
			return
		}
		reqBody, err := io.ReadAll(req.Body)
		if err != nil {
			params = map[string]string{
				"reason": fmt.Sprint(err.Error()),
			}
			a.Err = &models.CustomError{
				Message:   "update user group failed",
				ErrorCode: int(lterror.ErrorCode_UPDATE_FAILED),
				Params:    params,
			}
			log.Errorf("get request from %s failed, reason;%s", path, err.Error())
			return
		}
		req.Body = io.NopCloser(bytes.NewBuffer(reqBody))
		reqParams := make(map[string]string)
		err = json.Unmarshal(reqBody, &reqParams)
		if err != nil {
			params = map[string]string{
				"reason": fmt.Sprint(err.Error()),
			}
			a.Err = &models.CustomError{
				Message:   "update user group failed",
				ErrorCode: int(lterror.ErrorCode_UPDATE_FAILED),
				Params:    params,
			}
			return
		}
		reportParams := map[string]string{"account_numbers": reqParams["account_number"]}
		reportParamsByte, _ := json.Marshal(reportParams)
		tradingDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/ltreport/*")
		reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, len(reportParamsByte))

		requestReportUrl := fmt.Sprintf("%s/ltreport/v1/reports/users-check", tradingDomain)
		var reportRes *http.Response
		reportRes, err = a.Client.Post(requestReportUrl, reqHeaders, reportParamsByte, mTLS)
		if err != nil {
			log.Errorf("error occurred during request to; %s, reason;%s", requestReportUrl, err.Error())
			return
		}
		resReportBody, err := io.ReadAll(reportRes.Body)
		if err != nil {
			log.Errorf("read res body failed, reason; %s", err.Error())
			return
		}
		var respReport []models.UserHasOrder
		err = json.Unmarshal(resReportBody, &respReport)
		if err != nil {
			log.Errorf("error unmarshalling response from: %s, reason;%s", requestReportUrl, err.Error())
			return
		}
		if len(respReport) == 0 || respReport[0].HasOrder > 0 {
			a.Err = &models.CustomError{
				Message:   "When accounts have open position, pending order cannot change to another group",
				ErrorCode: int(lterror.ErrorCode_CAN_NOT_EDIT_GROUP_DEU_TO_OPENING_ORDER),
				Params:    params,
			}
			return
		}
		AccountDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/ltaccount/*")
		reqAccountHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, len(reqParams["account_number"]))
		accountNumber := reqParams["account_number"]
		requestAccountUrl := fmt.Sprintf("%s/ltaccount/v1/admin/traders/multi?query=%s", AccountDomain, accountNumber)
		var accountRes *http.Response

		accountRes, err = a.Client.Get(requestAccountUrl, reqAccountHeaders, mTLS)
		if err != nil {
			params = map[string]string{
				"reason": fmt.Sprint(err.Error()),
			}
			a.Err = &models.CustomError{
				Message:   fmt.Sprintf("get trader info failed, reason; {%s}", err.Error()),
				ErrorCode: int(lterror.ErrorCode_GET_TRADER_INFO_FAILED),
				Params:    params,
			}
			log.Errorf("get trader info failed, reason;%s", err.Error())
			return
		}
		resAccountBody, err := io.ReadAll(accountRes.Body)
		if err != nil {
			log.Errorf("get response from %s failed, reason;%s", requestAccountUrl, err.Error())
			return
		}
		var resAccountSlice []models.TraderAccountInfo
		err = json.Unmarshal(resAccountBody, &resAccountSlice)
		if err != nil {
			log.Errorf("error unmarshalling response from: %s, reason; %s", requestAccountUrl, err.Error())
			return
		}
		if len(resAccountSlice) == 0 {
			a.Err = &models.CustomError{
				Message:   "account information is empty, and can not find currency",
				ErrorCode: int(lterror.ErrorCode_ACCOUNT_INFO_IS_EMPTY),
				Params:    params,
			}
			log.Errorf("account information is empty, and can not find currency")
			return
		}
		firstAccount := resAccountSlice[0]

		reqParams["currency"] = firstAccount.Currency
		reqParamsByte, err := json.Marshal(reqParams)
		if err != nil {
			log.Errorf("Failed to marshal reqParams: %s", err.Error())
			return
		}

		req.Body = io.NopCloser(bytes.NewReader(reqParamsByte))
		req.ContentLength = int64(len(reqParamsByte))
		a.Err = nil
	}

	// Read the request body
	bodyBytes, err := io.ReadAll(ctx.Request().Body)
	if err != nil {
		log.Errorf("read body failed from context")
		return
	}

	// Store the request body in your variable
	a.ReqBody = bodyBytes

	// becase whenever we read it, it will be cleared
	// so need to rewrite to original request body again
	ctx.Request().Body = io.NopCloser(bytes.NewReader(bodyBytes))
}
func (a *SymbolGroupController) GetError() error {
	if a.Err != nil {
		jsonString, err := json.Marshal(a.Err)
		if err != nil {
			return fmt.Errorf("failed to convert error to string: %s", err)
		}
		return errors.New(string(jsonString))
	}
	return nil
}
