package controller

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"zerologix.com/interview/ltconst"
	"zerologix.com/interview/models"
	"zerologix.com/interview/service/httpclient"
)

type CreateTraderController struct {
	Ctx         echo.Context
	Client      httpclient.IHttpClient
	ReqBody     []byte
	TmpResult   []byte
	OriginQuery string
}

func (a *CreateTraderController) RewriteResponse(res *http.Response) error {
	// Step 1: create ltaccount.trader_info
	template := ResponseWriterTemplate{callPath: a.Ctx.Path()}
	if err := template.rewriteResTemplate(res, func(body []byte) ([]byte, error) {
		if res.StatusCode != http.StatusOK {
			log.Errorf("incorrect account response status code;%d", res.StatusCode)
			return body, nil
		}

		originQuery, err := url.ParseQuery(a.OriginQuery)
		if err != nil {
			return body, err
		}

		var traderlistBytes []byte
		if res.Request.Method == "GET" {
			if originQuery.Get("field") == "balance" || originQuery.Get("field") == "floating" {
				traderlistBytes, err = a.addLtreportAccountColToTraderList_sortbyLtreport(body)
			} else {
				traderlistBytes, err = a.addLtreportAccountColToTraderList_sortbyLtaccount(body)
			}
			if err != nil {
				log.Errorf("failed to add col, reason;%s", err.Error())
			}

			traderlistBytes, err := a.addLtsymbolGroupColToTraderList(traderlistBytes)
			return traderlistBytes, err
		}

		//below is POST means create account
		var originalBody map[string]interface{}
		if err := json.Unmarshal(a.ReqBody, &originalBody); err != nil {
			log.Errorf("unmarshal request body failed, reason;%s", err.Error())
			return body, err
		}
		currency, _ := originalBody["currency"].(string)
		deposit, _ := originalBody["deposit"].(string)
		symbolGroupId, _ := originalBody["symbol_group_id"].(string)

		modifiedBody := make(map[string]interface{}, 3)
		modifiedBody["currency"] = currency
		modifiedBody["deposit"] = deposit
		modifiedBody["symbol_group_id"] = symbolGroupId

		accountRes := make(map[string]interface{})
		err = json.Unmarshal(body, &accountRes)
		if err != nil {
			log.Errorf("unmarshal account response failed, reason;%s", err.Error())
			return body, err
		}

		// Step 2: create ltsymbol.account_symbol_group
		accountNumber, ok := accountRes["account_number"].(string)
		symbolGroupID, _ := originalBody["symbol_group_id"].(string)
		if len(symbolGroupID) > 0 {
			createSGBody := make(map[string]interface{}, 2)
			createSGBody["account_number"] = accountNumber
			createSGBody["symbol_group_id"] = symbolGroupID
			createSGBody["currency"] = currency
			log.Infof("modifiedBody; %s", createSGBody)
			createSGBodyByte, err := json.Marshal(createSGBody)
			if err != nil {
				log.Errorf("marshal create account symbol group body failed, reason;%s", err.Error())
				return body, err
			}
			symbolsDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/ltsymbols/*")
			reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, len(createSGBody))
			requestUrl := fmt.Sprintf("%s/ltsymbols/v1/admin/accountSymbolGroup", symbolsDomain)
			tradersRes, err := a.Client.Post(requestUrl, reqHeaders, createSGBodyByte, mTLS)
			if err != nil {
				err = fmt.Errorf("create ltsymbols.account_symbol_group failed, reason;%s", err.Error())
				log.Errorf(err.Error())

				// delete trader_info
				_ = a.deleteTrader(accountNumber)

				res.StatusCode = http.StatusBadRequest
				return json.Marshal(models.Message{Message: err.Error()})
			}
			if tradersRes.StatusCode != http.StatusOK {
				err = fmt.Errorf("create ltsymbols.account_symbol_group fail, status code;%d", tradersRes.StatusCode)
				log.Errorf(err.Error())

				// delete trader_info
				_ = a.deleteTrader(accountNumber)

				res.StatusCode = http.StatusBadRequest
				return json.Marshal(models.Message{Message: err.Error()})
			}
		}

		// Step 3: create lttrading.accounts
		if ok {
			modifiedBody["account_number"] = accountNumber
		}
		log.Infof("modifiedBody; %s", modifiedBody)
		modifiedBodyByte, err := json.Marshal(modifiedBody)
		if err != nil {
			log.Errorf("marshal modified body failed, reason;%s", err.Error())
			return body, err
		}

		tradingDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/lttrading/*")
		reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, len(modifiedBody))

		requestUrl := fmt.Sprintf("%s/lttrading/v1/admin/traders", tradingDomain)
		tradersRes, err := a.Client.Post(requestUrl, reqHeaders, modifiedBodyByte, mTLS)
		if err != nil {
			err = fmt.Errorf("create lttrading.accounts failed, reason;%s", err.Error())
			log.Errorf(err.Error())

			// delete trader_info
			_ = a.deleteTrader(accountNumber)

			res.StatusCode = http.StatusBadRequest
			return json.Marshal(models.Message{Message: err.Error()})
		}
		if tradersRes.StatusCode != http.StatusOK {
			err = fmt.Errorf("create lttrading.accounts fail response status code; %d", tradersRes.StatusCode)
			log.Errorf(err.Error())

			// delete trader_info
			_ = a.deleteTrader(accountNumber)

			res.StatusCode = http.StatusBadRequest
			return json.Marshal(models.Message{Message: err.Error()})
		}

		//step 4 : notify ltquoting new account symbol group added
		responseBody := []map[string]interface{}{}
		responseBody = append(responseBody, map[string]interface{}{"group_id": symbolGroupID, "account_number": accountNumber, "action": ltconst.ActionAdd})

		modifiedBodyByte, err = json.Marshal(responseBody)
		if err != nil {
			log.Errorf("marshal notify ltquoting new account symbol group added modified body failed, reason;%s", err.Error())
		} else {

			var quotingRes *http.Response
			quotingDomain, quotingMTLS := GetDomainAndMTLS(a.Ctx, "/ltquoting/*")
			quotingReqUrl := fmt.Sprintf("%s/ltquoting/v1/admin/accountSymbolGroup", quotingDomain)

			quotingRes, err = a.Client.Post(quotingReqUrl, reqHeaders, modifiedBodyByte, quotingMTLS)
			if err != nil {
				failedAccId := responseBody[0]["account_number"]
				failedGroupId := responseBody[0]["group_id"]
				failedAction := responseBody[0]["action"]
				log.Errorf("notify ltquoting failed, account_number; %v, symbol_group_id; %v, action; %v, reason;%s", failedAccId, failedGroupId, failedAction, err.Error())
			} else {
				if quotingRes.StatusCode != 200 {
					log.Errorf("incorrect ltquoting response status code;%d", quotingRes.StatusCode)
				} else {
					log.Infof("notify ltquoting account symbol group successfully")
				}
			}
		}

		//step 5: all success then assemble new response body
		assembleCreateAccountResponse := models.AssembleCreateAccountResponse{}
		err = json.Unmarshal(body, &assembleCreateAccountResponse)
		if err != nil {
			log.Errorf("unmarshal assembleCreateAccountResponse response failed, reason;%s", err.Error())
			return body, err
		}
		ltsymbolsDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/ltsymbols/*")
		requestUrl = fmt.Sprintf("%s/ltsymbols/v1/admin/accountSymbolGroup?query=%s", ltsymbolsDomain, accountNumber)
		accountSymbolGroup, err := a.Client.Get(requestUrl, reqHeaders, mTLS)
		if err != nil {
			err = fmt.Errorf("get ltsymbols.accountSymbolGroup failed, reason;%s", err.Error())
			log.Errorf(err.Error())
			res.StatusCode = http.StatusBadRequest
			return json.Marshal(models.Message{Message: err.Error()})
		}
		accountSymbolGroupBody, err := io.ReadAll(accountSymbolGroup.Body)
		if err != nil {
			log.Errorf("read accountSymbolGroupBody info response body failed, reason;%s", err.Error())
		}
		accountSymbolGroupQueryResponseArray := []models.AccountSymbolGroupQueryResponse{}
		err = json.Unmarshal(accountSymbolGroupBody, &accountSymbolGroupQueryResponseArray)
		if err != nil {
			log.Errorf("unmarshal accountSymbolGroupQueryResponse response failed, reason;%s", err.Error())
			return body, err
		}
		if len(accountSymbolGroupQueryResponseArray) > 0 {
			if accountSymbolGroupQueryResponseArray[0].AccountNumber == assembleCreateAccountResponse.AccountNumber {
				assembleCreateAccountResponse.Group = models.GroupListResponse{
					ID:              strconv.FormatInt(accountSymbolGroupQueryResponseArray[0].GroupID, 10),
					Name:            accountSymbolGroupQueryResponseArray[0].GroupName,
					BaseCurrency:    accountSymbolGroupQueryResponseArray[0].BaseCurrency,
					MarginCallLevel: accountSymbolGroupQueryResponseArray[0].MarginCallLevel,
					StopOutLevel:    accountSymbolGroupQueryResponseArray[0].StopOutLevel,
				}
				//all success
				accountServiceDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/ltaccount/*")
				reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, 10)
				requestUrl := fmt.Sprintf("%s/ltaccount/v1/admin/traders/sendemail/%s", accountServiceDomain, accountNumber)
				accountRes, err := a.Client.Get(requestUrl, reqHeaders, mTLS)
				if err != nil {
					err = fmt.Errorf("sending create account email fail reason;%s", err.Error())
					log.Errorf(err.Error())
					res.StatusCode = http.StatusBadRequest
					return json.Marshal(models.Message{Message: err.Error()})
				}
				if accountRes.StatusCode != http.StatusOK {
					err = fmt.Errorf("sending create account email fail response status code;%d", tradersRes.StatusCode)
					log.Errorf(err.Error())
					res.StatusCode = http.StatusBadRequest
					return json.Marshal(models.Message{Message: err.Error()})
				}
				return json.Marshal(assembleCreateAccountResponse)
			}
		}

		log.Debugf("unexpected error but still return basic success body")
		return body, nil
	}); err != nil {
		log.Debugf("rewrite response failed, reason;%s", err.Error())
		return err
	}
	return nil
}

func (a *CreateTraderController) assembleValidHeaders(echoReqHeaders map[string][]string, contentLength int) map[string]string {
	validHeaders := make(map[string]string)
	for key, value := range echoReqHeaders {
		if key != "Content-Length" {
			validHeaders[key] = value[0]
		}
	}
	validHeaders["Content-Length"] = strconv.Itoa(contentLength)
	return validHeaders
}
func (a *CreateTraderController) InjectCtx(ctx echo.Context) {
	a.Ctx = ctx

	tmpField := ctx.QueryParams().Get("field")
	a.OriginQuery = ctx.QueryParams().Encode()
	if tmpField == "balance" || tmpField == "floating" {
		ctx.QueryParams().Set("field", "")
		ctx.Request().URL.RawQuery = ctx.QueryParams().Encode()

		if len(ctx.QueryParams().Get("query")) > 0 {
			//sorting with query
			//force to get all
			ctx.QueryParams().Set("limit", "1000")
			ctx.QueryParams().Set("page", "1")
		}
		ctx.Request().URL.RawQuery = ctx.QueryParams().Encode()
	}
	//handle special situation
	//for GET traders online
	//1. check request if include query "online=1"
	if ctx.Request().Method == http.MethodGet {
		isOnline := ctx.Request().URL.Query().Get("online")
		if len(isOnline) > 0 {
			//2. get online account_number by other API
			tradingServiceDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/lttrading/*")
			requestUrl := fmt.Sprintf("%s/lttrading/v1/admin/traders", tradingServiceDomain)
			reqHeaders := a.assembleValidHeaders(ctx.Request().Header, 10)
			tradersRes, err := a.Client.Get(requestUrl, reqHeaders, mTLS)
			if err != nil {
				log.Errorf("get traders info failed, reason;%s", err.Error())
			}
			if tradersRes.StatusCode != 200 {
				log.Errorf("incorrect traders info response status code;%d", tradersRes.StatusCode)
			}
			tradersInfoBody, err := io.ReadAll(tradersRes.Body)
			if err != nil {
				log.Errorf("read traders info response body failed, reason;%s", err.Error())
			}
			var tradersInfo []int
			if err := json.Unmarshal(tradersInfoBody, &tradersInfo); err != nil {
				log.Errorf("unmarshal trader account info res body failed, reason;%s", err.Error())
			}
			_ = tradersRes.Body.Close()
			accountNumber := "0" //means not found
			if len(tradersInfo) > 0 {
				//3. pass online account_number thought
				strNums := make([]string, len(tradersInfo))
				for i, num := range tradersInfo {
					strNums[i] = strconv.Itoa(num)
				}
				accountNumber = strings.Join(strNums, ",")
			}
			orginQuery := a.Ctx.QueryParams()
			orginQuery.Set("within_account_number", accountNumber)
			a.Ctx.Request().URL.RawQuery = orginQuery.Encode()
		}
	}

	// Read the request body
	bodyBytes, err := io.ReadAll(ctx.Request().Body)
	if err != nil {
		log.Errorf("read body failed from context")
		return
	}

	// Store the request body in your variable
	a.ReqBody = bodyBytes

	// becase whenever we read it, it will be cleared
	// so need to rewrite to original request body again
	ctx.Request().Body = io.NopCloser(bytes.NewReader(bodyBytes))

	//handle special sorting
}

func (a *CreateTraderController) addLtsymbolGroupColToTraderList(body []byte) ([]byte, error) {
	//1. unmarshal the account list data from ltaccounts
	bodylist := models.TraderListWithPage{}
	if err := json.Unmarshal(body, &bodylist); err != nil {
		log.Errorf("unmarshal request body failed, reason;%s", err.Error())
		return body, err
	}
	if len(bodylist.Data) == 0 {
		return body, nil
	}
	accountnumbers := ""
	for _, v := range bodylist.Data {
		// accountnumbers += v.AccountNumber + ","
		if accountnumbers == "" {
			accountnumbers = v.AccountNumber
		} else {
			accountnumbers += "," + v.AccountNumber
		}
	}

	// 2. Add 2 columns "group id " "group name" from ltreport > lttrading's accounts table
	// request 2 new columns info via ltsymbols
	// {{domain}}/ltsymbols/v1/admin/accountSymbolGroup?query=11111,12233,34234314
	ltsymbolsDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/ltsymbols/*")
	reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, 10)
	// requestUrl := fmt.Sprintf("%s/ltsymbols/v1/admin/accountSymbolGroup?limit=1000&query=%s", ltsymbolsDomain, accountnumbers)
	requestUrl := fmt.Sprintf("%s/ltsymbols/v1/admin/accountSymbolGroup?query=%s", ltsymbolsDomain, accountnumbers)
	accountGroupRes, err := a.Client.Get(requestUrl, reqHeaders, mTLS)
	if err != nil {
		log.Errorf("get account's group data failed, reason;%s", err.Error())
		return body, err
	}
	if accountGroupRes.StatusCode != 200 {
		log.Errorf("incorrect account's group data response status code;%d", accountGroupRes.StatusCode)
		return body, fmt.Errorf("incorrect account's group data response status code;%d", accountGroupRes.StatusCode)
	}

	accountGroupBodyBytes, err := io.ReadAll(accountGroupRes.Body)
	if err != nil {
		log.Errorf("read account's group data response body failed, reason;%s", err.Error())
		return body, err
	}

	groupres := []models.GetSymbolGroupReponse{}
	if err := json.Unmarshal(accountGroupBodyBytes, &groupres); err != nil {
		log.Errorf("unmarshal account's group body bytes failed, reason;%s", err.Error())
		return body, err
	}

	//update group fields in the account list (skip this part if no group data is found for these trader accounts)
	if len(groupres) > 0 {
		accgroupmap := make(map[string]models.GetSymbolGroupReponse, len(groupres))
		for _, v := range groupres {
			accgroupmap[v.AccountNumber] = v
		}

		for idx, v := range bodylist.Data {
			emptyRes := models.GetSymbolGroupReponse{}
			if accgroupmap[v.AccountNumber] != emptyRes {
				bodylist.Data[idx].GroupId = strconv.FormatUint(accgroupmap[v.AccountNumber].GroupID, 10)
				bodylist.Data[idx].GroupName = accgroupmap[v.AccountNumber].GroupName
			}
		}
	}

	return json.Marshal(bodylist)
}

func (a *CreateTraderController) addLtreportAccountColToTraderList_sortbyLtaccount(body []byte) ([]byte, error) {

	//1. unmarshal the account list data from ltaccounts
	bodylist := models.TraderListWithPage{}
	if err := json.Unmarshal(body, &bodylist); err != nil {
		log.Errorf("unmarshal request body failed, reason;%s", err.Error())
		return body, err
	}
	if len(bodylist.Data) == 0 {
		return body, nil
	}
	accountnumbers := ""
	for _, v := range bodylist.Data {
		// accountnumbers += v.AccountNumber + ","
		if accountnumbers == "" {
			accountnumbers = v.AccountNumber
		} else {
			accountnumbers += "," + v.AccountNumber
		}
	}

	// 2. Add 2 columns "balance" "floating" from ltreport > lttrading's accounts table
	// request 2 new columns info via ltreport
	reportDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/ltreport/*")
	reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, 10)
	requestUrl := fmt.Sprintf("%s/ltreport/v1/admin/accounts?limit=1000&query=%s", reportDomain, accountnumbers)
	accountRes, err := a.Client.Get(requestUrl, reqHeaders, mTLS)
	if err != nil {
		log.Errorf("get account info failed, reason;%s", err.Error())
		return body, err
	}
	if accountRes.StatusCode != 200 {
		log.Errorf("incorrect account info response status code;%d", accountRes.StatusCode)
		return body, fmt.Errorf("incorrect account info response status code;%d", accountRes.StatusCode)
	}
	accountInfoBody, err := io.ReadAll(accountRes.Body)
	if err != nil {
		log.Errorf("read account info response body failed, reason;%s", err.Error())
		return body, err
	}
	accountlist := models.AccountListWithPage{}
	if err := json.Unmarshal(accountInfoBody, &accountlist); err != nil {
		log.Errorf("unmarshal request body failed, reason;%s", err.Error())
		return body, err
	}

	// merge 2 new columns info to list
	tmpList := make(map[string]*models.AccountList, len(accountlist.Data))
	for _, v := range accountlist.Data {
		tmpList[v.AccountNumber] = v
	}
	for _, v := range bodylist.Data {
		tmp := tmpList[v.AccountNumber]
		if tmp != nil {
			v.Balance = tmp.Balance
			v.Floating = tmp.Floating
		}
	}
	return json.Marshal(bodylist)
}

func (a *CreateTraderController) addLtreportAccountColToTraderList_sortbyLtreport(body []byte) ([]byte, error) {

	copyQuery, err := url.ParseQuery(a.OriginQuery)
	if err != nil {
		log.Errorf("parse query reason;%s", err.Error())
		return body, err
	}
	if len(copyQuery.Get("query")) > 0 {
		//sorting with query
		tmpBody, err := a.addLtreportAccountColToTraderList_sortbyLtaccount(body)
		if err != nil {
			log.Errorf("can not parse trader list body reason;%s", err.Error())
			return body, err
		}
		return a.handleSorting(tmpBody)
	}

	//sorting without query
	//call ltreport api instead
	reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, 10)
	reportDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/ltreport/*")
	requestUrl := fmt.Sprintf("%s/ltreport/v1/admin/accounts?%s", reportDomain, a.OriginQuery)
	accountRes, err := a.Client.Get(requestUrl, reqHeaders, mTLS)
	if err != nil {
		log.Errorf("get account info failed, reason;%s", err.Error())
		return body, err
	}
	if accountRes.StatusCode != 200 {
		log.Errorf("incorrect account info response status code;%d", accountRes.StatusCode)
		return body, err
	}
	accountInfoBody, err := io.ReadAll(accountRes.Body)
	a.TmpResult = accountInfoBody
	if err != nil {
		log.Errorf("read account info response body failed, reason;%s", err.Error())
		return body, err
	}
	accountlist := models.TraderListWithPage{}
	if err := json.Unmarshal(accountInfoBody, &accountlist); err != nil {
		log.Errorf("unmarshal request body failed, reason;%s", err.Error())
		return body, err
	}
	if err != nil {
		log.Errorf("get traders info failed, reason;%s", err.Error())
		return body, err
	}
	//get all accounts
	accounts := ""
	for _, v := range accountlist.Data {
		accounts += v.AccountNumber + ","
	}

	accountListTmp := models.TraderListWithPage{}
	if len(accounts) > 0 {
		accountServiceDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/ltaccount/*")
		reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, 10)
		requestUrl := fmt.Sprintf("%s/ltaccount/v1/admin/traders?query=%s", accountServiceDomain, accounts)
		accountRes, err := a.Client.Get(requestUrl, reqHeaders, mTLS)
		if err != nil {
			log.Errorf("can not GET account list by multiple accounts, reason;%s", err.Error())
			return body, err
		}
		accountInfoBody, err := io.ReadAll(accountRes.Body)
		if err != nil {
			log.Errorf("read account info response body failed, reason;%s", err.Error())
			return body, err
		}
		if err := json.Unmarshal(accountInfoBody, &accountListTmp); err != nil {
			log.Errorf("unmarshal request body failed, reason;%s", err.Error())
			return body, err
		}
		if err != nil {
			log.Errorf("get traders info failed, reason;%s", err.Error())
			return body, err
		}
		tmp := make(map[string]*models.TraderList, len(accountListTmp.Data))
		for _, v := range accountListTmp.Data {
			tmp[v.AccountNumber] = v
		}
		for key, value := range accountlist.Data {
			if v, ok := tmp[value.AccountNumber]; ok {
				accountlist.Data[key].UID = v.AccountNumber
				accountlist.Data[key].Country = v.Country
				accountlist.Data[key].CreatedAt = v.CreatedAt
				accountlist.Data[key].Currency = v.Currency
				accountlist.Data[key].Email = v.Email
				accountlist.Data[key].Firstname = v.Firstname
				accountlist.Data[key].Lang = v.Lang
				accountlist.Data[key].Lastname = v.Lastname
				accountlist.Data[key].LastVisited = v.LastVisited
				accountlist.Data[key].PhoneNumber = v.PhoneNumber
				accountlist.Data[key].UpdatedAt = v.UpdatedAt
			}
		}
	}
	return json.Marshal(accountlist)
}

func (a *CreateTraderController) handleSorting(body []byte) ([]byte, error) {
	bodylist := models.TraderListWithPage{}
	if err := json.Unmarshal(body, &bodylist); err != nil {
		log.Errorf("unmarshal request body failed, reason;%s", err.Error())
		return body, err
	}
	if len(bodylist.Data) == 0 {
		return body, nil
	}

	copyQuery, err := url.ParseQuery(a.OriginQuery)
	if err != nil {
		log.Errorf("parse query reason;%s", err.Error())
		return body, err
	}
	switch copyQuery.Get("field") {
	case "balance":
		if copyQuery.Get("sort") == "asc" {
			sort.Sort(ByBalance(bodylist.Data))
		} else {
			sort.Sort(sort.Reverse(ByBalance(bodylist.Data)))
		}
	case "floating":
		if copyQuery.Get("sort") == "asc" {
			sort.Sort(ByFloating(bodylist.Data))
		} else {
			sort.Sort(sort.Reverse(ByFloating(bodylist.Data)))
		}
	}
	return json.Marshal(bodylist)
}

func (a *CreateTraderController) deleteTrader(accountNumber string) error {
	tradingDomain, mTLS := GetDomainAndMTLS(a.Ctx, "/ltaccount/*")
	reqHeaders := a.assembleValidHeaders(a.Ctx.Request().Header, 10)
	requestUrl := fmt.Sprintf("%s/ltaccount/v1/admin/traders/%s", tradingDomain, accountNumber)

	traderDelRes, err := a.Client.Delete(requestUrl, reqHeaders, mTLS)
	if err != nil {
		log.Errorf("remove lttrading.accounts failed, reason;%s", err.Error())
		return err
	}
	if traderDelRes.StatusCode != 200 {
		err = fmt.Errorf("remove lttrading.accounts failed response status code;%d", traderDelRes.StatusCode)
		log.Errorf(err.Error())
		return err
	}
	return nil
}

type ByBalance []*models.TraderList

func (s ByBalance) Len() int {
	return len(s)
}
func (s ByBalance) Less(i, j int) bool {
	iBalance, _ := strconv.ParseFloat(s[i].Balance, 64)
	jBalance, _ := strconv.ParseFloat(s[j].Balance, 64)
	return iBalance < jBalance
}
func (s ByBalance) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

type ByFloating []*models.TraderList

func (s ByFloating) Len() int {
	return len(s)
}
func (s ByFloating) Less(i, j int) bool {
	iFloating, _ := strconv.ParseFloat(s[i].Floating, 64)
	jFloting, _ := strconv.ParseFloat(s[j].Floating, 64)
	return iFloating < jFloting
}
func (s ByFloating) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (a *CreateTraderController) GetError() error {
	return nil
}
