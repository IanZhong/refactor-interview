package routing

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"gopkg.in/yaml.v3"
	"zerologix.com/interview/controller"
	"zerologix.com/interview/ltconst"
	"zerologix.com/interview/models"
	"zerologix.com/interview/service"
	"zerologix.com/interview/service/httpclient"
)

type CertPoolInterface interface {
	AppendCertsFromPEM(pemCerts []byte) (ok bool)
}

type RuleConfig struct {
	Rules map[string]models.PathInfo
	echo  *echo.Echo

	certPool   *x509.CertPool
	clientCert tls.Certificate

	proxyPoolMap map[string]*service.ReverseProxyPool
}

func (c *RuleConfig) GetDomainAndMTLS(key string) (string, bool) {
	rule := c.Rules[key]
	return rule.Target, strings.EqualFold(rule.MTLS, "on")
}

func (c *RuleConfig) injectPathHandler() {
	for path, info := range c.Rules {
		if info.Proxy == "general" {
			continue
		}

		switch path {
		case "/ltaccount/v1/admin/traders":
			c.injectSpecialLogic(path, &controller.CreateTraderController{
				Client: httpclient.SingleHttpClient(),
			})
		case "/ltsymbols/v1/admin/groups":
			c.injectSpecialLogic(path, &controller.SymbolGroupController{
				Client: httpclient.SingleHttpClient(),
			})
		case "/ltsymbols/v1/admin/group":
			c.injectSpecialLogic(path, &controller.SymbolGroupController{
				Client: httpclient.SingleHttpClient(),
			})
		case "/ltsymbols/v1/admin/groupSymbols":
			c.injectSpecialLogic(path, &controller.SymbolGroupController{
				Client: httpclient.SingleHttpClient(),
			})
		case "/ltsymbols/v1/admin/groupSymbol":
			c.injectSpecialLogic(path, &controller.SymbolGroupController{
				Client: httpclient.SingleHttpClient(),
			})
		case "/ltsymbols/v1/admin/accountSymbolGroup":
			c.injectSpecialLogic(path, &controller.SymbolGroupController{
				Client: httpclient.SingleHttpClient(),
			})
		}
	}
}

func Initialize(e *echo.Echo, path string) (*RuleConfig, error) {
	c := &RuleConfig{
		echo:         e,
		certPool:     x509.NewCertPool(),
		proxyPoolMap: make(map[string]*service.ReverseProxyPool),
	}
	if err := c.initRoutingRulesFromYAML(path); err != nil {
		return nil, err
	}

	c.injectPathHandler()

	disableMTLS := os.Getenv("DISABLE_MTLS") == "true"

	c.certPool = httpclient.GetCertPool()
	c.clientCert = httpclient.GetClientCert()

	for rule, info := range c.Rules {
		mTLS := false
		if strings.EqualFold(info.MTLS, "on") && !disableMTLS {
			mTLS = true
		}
		if strings.EqualFold(info.Proxy, "general") {
			c.registryGeneralRouting(rule, info.Target, mTLS)
		} else if strings.EqualFold(info.Proxy, "special") {
			c.registerSpecialRouting(rule, info.Target, mTLS)
		} else {
			c.registerCustomizedHandler(rule, info.Target, mTLS)
		}
	}
	if disableMTLS {
		return c, nil
	}
	return c, nil
}

func (c *RuleConfig) initRoutingRulesFromYAML(path string) error {
	yamlFile, err := os.ReadFile(path)
	if err != nil {
		log.Errorf("failed to read routing config file, reason;%s ", err.Error())
		return err
	}
	if err = yaml.Unmarshal(yamlFile, c); err != nil {
		log.Errorf("failed to unmarshal routing config, reason;%s", err.Error())
		return err
	}
	log.Infof("init routing rules from yaml, path;%s, rules;%v", path, c.Rules)
	return nil
}

func (c *RuleConfig) registryGeneralRouting(path string, targetURI string, mTLS bool) {
	c.echo.Any(
		path,
		c.reverseProxy(targetURI, mTLS),
	)
}

func (c *RuleConfig) injectSpecialLogic(path string, handler LogicRegister) {
	SingleSpecialProxyLogicHub().AddRouting(path, handler)
}

func (c *RuleConfig) injectCustomizedLogic(path string, handler func(ctx echo.Context) error) {
	target := c.Rules[path].Target
	SingleSpecialProxyLogicHub().AddCustomizedHandler(path, target, func(ctx echo.Context) error {
		if ctx.Request().Method == "OPTIONS" {
			return c.generalHandler(target)(ctx)
		}
		return handler(ctx)
	})
}

func (c *RuleConfig) registerSpecialRouting(path string, targetURI string, mTLS bool) {
	c.echo.Any(
		path,
		c.specialProxy(targetURI, mTLS),
	)
}

func (c *RuleConfig) registerCustomizedHandler(path string, targetURI string, mTLS bool) {
	c.echo.Any(path, c.customizedHandler(path, mTLS))
}

// create a reverse proxy that forwards requests to a backend service
func (c *RuleConfig) reverseProxy(target string, mTLS bool) echo.HandlerFunc {
	if err := c.initProxyPool(target, mTLS); err != nil {
		return nil
	}
	return c.generalHandler(target)
}

// create a reverse proxy that forwards requests to a backend service
func (c *RuleConfig) specialProxy(target string, mTLS bool) echo.HandlerFunc {
	if err := c.initProxyPool(target, mTLS); err != nil {
		return nil
	}
	return c.specialHandler(target)
}

func (c *RuleConfig) customizedHandler(target string, mTLS bool) echo.HandlerFunc {
	return SingleSpecialProxyLogicHub().GetCustomizedHandler(target)
}

// initialise reverse proxy pool, create reverse proxy pool for each target
func (c *RuleConfig) initProxyPool(target string, mTLS bool) error {
	if _, ok := c.proxyPoolMap[target]; !ok {
		backendURL, _ := url.Parse(target)
		if mTLS {
			backendURL.Scheme = "https"
		}

		proxyPool := service.NewReverseProxyPool(backendURL, mTLS, c.certPool, c.clientCert, getProxyPoolSize())
		c.proxyPoolMap[target] = proxyPool
	}
	return nil
}

func (c *RuleConfig) generalHandler(target string) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		// get reverse proxy from proxy pool
		proxy := c.proxyPoolMap[target].GetReverseProxy()
		if proxy == nil {
			return errors.New("no available proxy connection")
		}
		defer func() {
			proxy.ModifyResponse = nil
			c.proxyPoolMap[target].ReleaseReverseProxy(proxy)
		}()

		// serve the request via the proxy
		proxy.ServeHTTP(ctx.Response().Writer, ctx.Request())

		return nil
	}
}

func (c *RuleConfig) specialHandler(target string) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		if ctx.Request().Method == "OPTIONS" {
			return c.generalHandler(target)(ctx)
		}

		log.Debugf("Header from echo context %v", ctx.Request().Header.Get("authorization"))

		// get reverse proxy from proxy pool
		proxy := c.proxyPoolMap[target].GetReverseProxy()
		if proxy == nil {
			return errors.New("no available proxy connection")
		}
		defer func() {
			proxy.ModifyResponse = nil
			c.proxyPoolMap[target].ReleaseReverseProxy(proxy)
		}()

		// update the request URL to match the backend service
		// handler mapping key is original url path
		handler := SingleSpecialProxyLogicHub().GetRoutingLogic(ctx.Path())
		ctx.Set(ltconst.CTX__PATH_RULE_KEY, c.Rules)
		if handler != nil {
			handler.InjectCtx(ctx)
			if err := handler.GetError(); err != nil {
				return err
			}
		} else {
			log.Warnf("route logic not found for path;%s", ctx.Path())
		}
		proxy.ModifyResponse = handler.RewriteResponse

		// serve the request via the proxy
		proxy.ServeHTTP(ctx.Response().Writer, ctx.Request())

		return nil
	}
}

func getProxyPoolSize() int {
	size, err := strconv.Atoi(os.Getenv("PROXY_POOL_SIZE"))
	if err != nil {
		return ltconst.ReverseProxyPoolSize
	}
	return size
}
