package routing

import (
	"net/http"
	"strings"
	"sync"

	"github.com/labstack/echo/v4"
)

type LogicRegister interface {
	RewriteResponse(res *http.Response) error
	InjectCtx(ctx echo.Context)
	GetError() error
}

var specialProxyLogicHub *SpecialProxyLogicHub
var initSpecialProxyLogicHub sync.Once

func SingleSpecialProxyLogicHub() *SpecialProxyLogicHub {
	initSpecialProxyLogicHub.Do(func() {
		if specialProxyLogicHub == nil {
			specialProxyLogicHub = &SpecialProxyLogicHub{
				routingLogicMap:   map[string]LogicRegister{},
				customizedHandler: make(map[string]func(ctx echo.Context) error),
			}
		}
	})
	return specialProxyLogicHub
}

type SpecialProxyLogicHub struct {
	routingLogicMap   map[string]LogicRegister
	customizedHandler map[string]func(ctx echo.Context) error
}

func (s *SpecialProxyLogicHub) AddRouting(path string, logic LogicRegister) {
	validPath := path
	// ex: xxx/xxx/:id
	if strings.Contains(path, ":") {
		colonIndex := strings.LastIndex(validPath, ":")
		validPath = validPath[:colonIndex-1] // minus 1, because it doesn't need last '/'
	}
	s.routingLogicMap[validPath] = logic
}

func (s *SpecialProxyLogicHub) AddCustomizedHandler(path string, target string, h func(ctx echo.Context) error) {
	validPath := path
	// ex: xxx/xxx/:id
	if strings.Contains(path, ":") {
		colonIndex := strings.LastIndex(validPath, ":")
		validPath = validPath[:colonIndex-1] // minus 1, because it doesn't need last '/'
	}
	s.customizedHandler[validPath] = h
}

func (s *SpecialProxyLogicHub) GetRoutingLogic(path string) LogicRegister {
	validPath := path
	// ex: xxx/xxx/:id
	if strings.Contains(path, ":") {
		colonIndex := strings.LastIndex(validPath, ":")
		validPath = validPath[:colonIndex-1] // minus 1, because it doesn't need last '/'
	}
	return s.routingLogicMap[validPath]
}

func (s *SpecialProxyLogicHub) GetCustomizedHandler(path string) func(ctx echo.Context) error {
	validPath := path
	// ex: xxx/xxx/:id
	if strings.Contains(path, ":") {
		colonIndex := strings.LastIndex(validPath, ":")
		validPath = validPath[:colonIndex-1] // minus 1, because it doesn't need last '/'
	}
	return s.customizedHandler[validPath]
}
