package ltconst

import "time"

type SocketIOHandlerAction uint32

// definitions of content type for journal information
type JournalOrderType int8

const CTX__PATH_RULE_KEY = "rules"

const (
	ActionAdd        = "1"
	ActionUpdate     = "2"
	ActionDelete     = "3"
	ActionAllUpdate  = "4"
	ActionDisableTSL = "5"
)

const (
	RounterActionPost   = "POST"
	RounterActionGet    = "GET"
	RounterActionDelete = "DELETE"
	RounterActionPut    = "PUT"
)

const (
	ReverseProxyPoolSize  = 10
	DialAliveTime         = 30 * time.Second
	DialTimeout           = 30 * time.Second
	TLSHandshakeTimeout   = 10 * time.Second
	ResponseHeaderTimeout = 30 * time.Second
	ExpectContinueTimeout = 10 * time.Second
)
