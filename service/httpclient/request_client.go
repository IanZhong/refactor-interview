package httpclient

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/labstack/gommon/log"
)

type IHttpClient interface {
	Get(url string, headers map[string]string, mTLS bool) (*http.Response, error)
	// TBD: add other http methods support in the future
	Post(url string, headers map[string]string, data []byte, mTLS bool) (*http.Response, error)
	Delete(url string, headers map[string]string, mTLS bool) (*http.Response, error)
	Put(url string, headers map[string]string, data []byte, mTLS bool) (*http.Response, error)
}

type APIHttpClient struct {
	client     *http.Client
	mTLSClient *http.Client
}

var apiHttpClient *APIHttpClient
var initRestyClient sync.Once

func SingleHttpClient() IHttpClient {
	initRestyClient.Do(func() {
		if apiHttpClient == nil {
			apiHttpClient = &APIHttpClient{
				client: &http.Client{
					Timeout: 50 * time.Second,
				},
				mTLSClient: &http.Client{
					Transport: &http.Transport{
						TLSClientConfig: &tls.Config{
							RootCAs:      GetCertPool(),
							Certificates: []tls.Certificate{GetClientCert()},
						},
					},
				},
			}
		}
	})
	return apiHttpClient
}

func (a *APIHttpClient) Get(url string, headers map[string]string, mTLS bool) (res *http.Response, err error) {
	defer func() {
		if res == nil {
			log.Errorf("APIHttpClient Get failed, url%s, mTLS;%t, reason;%s", url, mTLS, err.Error())
			return
		}
		log.Debugf("APIHttpClient Get success, url;%s, mTLS;%t, status_code;%d, resp;%s", url, mTLS, res.StatusCode, res.Body)
	}()
	if mTLS {
		url = strings.Replace(url, "http", "https", 1)
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	for key, value := range headers {
		req.Header.Set(key, value)
	}
	client := a.client
	if mTLS {
		client = a.mTLSClient
	}
	res, err = client.Do(req)
	return
}

func (a *APIHttpClient) Post(url string, headers map[string]string, data []byte, mTLS bool) (res *http.Response, err error) {
	defer func() {
		if res == nil {
			return
		}
	}()
	if mTLS {
		url = strings.Replace(url, "http", "https", 1)
	}
	req, err := http.NewRequest("POST", url, bytes.NewReader(data))
	if err != nil {
		return nil, err
	}
	for key, value := range headers {
		req.Header.Set(key, value)
	}
	client := a.client
	if mTLS {
		client = a.mTLSClient
	}
	res, err = client.Do(req)
	return
}

func (a *APIHttpClient) Put(url string, headers map[string]string, data []byte, mTLS bool) (res *http.Response, err error) {
	defer func() {
		if res == nil {
			return
		}
	}()
	if mTLS {
		url = strings.Replace(url, "http", "https", 1)
	}
	req, err := http.NewRequest("PUT", url, bytes.NewReader(data))
	if err != nil {
		return nil, err
	}
	for key, value := range headers {
		req.Header.Set(key, value)
	}
	client := a.client
	if mTLS {
		client = a.mTLSClient
	}
	res, err = client.Do(req)
	return
}

func (a *APIHttpClient) Delete(url string, headers map[string]string, mTLS bool) (res *http.Response, err error) {
	defer func() {
		if res == nil {
			return
		}
	}()
	if mTLS {
		url = strings.Replace(url, "http", "https", 1)
	}
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}
	for key, value := range headers {
		req.Header.Set(key, value)
	}
	client := a.client
	if mTLS {
		client = a.mTLSClient
	}
	res, err = client.Do(req)
	return
}

const (
	certsDirPath = "config/certs/%s"

	ServerCAEnv   = "CERTIFICATE_CA_FILE"
	clientCertEnv = "CERTIFICATE_CRT_FILE"
	clientKeyEnv  = "CERTIFICATE_KEY_FILE"
)

func GetCertPool() *x509.CertPool {
	certPool := x509.NewCertPool()
	// read server certs from file and add them into cert pool
	serverCert := os.Getenv(ServerCAEnv)
	cert, err := os.ReadFile(fmt.Sprintf(certsDirPath, serverCert))
	if err != nil {
		log.Fatalf("failed open certificate file, reason;%s, server_cert_path;%s", err.Error(), fmt.Sprintf(certsDirPath, serverCert))
	}
	certPool.AppendCertsFromPEM(cert)
	return certPool
}

func GetClientCert() tls.Certificate {
	// read client certificate from file
	clientCert, err := tls.LoadX509KeyPair(fmt.Sprintf(certsDirPath, os.Getenv(clientCertEnv)), fmt.Sprintf(certsDirPath, os.Getenv(clientKeyEnv)))
	if err != nil {
		log.Fatalf("could not load certificate, reason;%s, client_cert_path;%s, client_key_path;%s", err.Error(), fmt.Sprintf(certsDirPath, os.Getenv(clientCertEnv)), fmt.Sprintf(certsDirPath, os.Getenv(clientKeyEnv)))
	}
	return clientCert
}
