package service

import (
	"crypto/tls"
	"crypto/x509"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"sync"

	"zerologix.com/interview/ltconst"
)

// ReverseProxyPool represent a pool of reverseProxies
type ReverseProxyPool struct {
	mu             sync.Mutex
	reverseProxies []*httputil.ReverseProxy
}

// NewReverseProxy create a new reverse proxy
func NewReverseProxy(backendURL *url.URL, mTLS bool, certPool *x509.CertPool, clientCert tls.Certificate) *httputil.ReverseProxy {
	reverseProxy := httputil.NewSingleHostReverseProxy(backendURL)

	// construct mTLS setting
	if mTLS {
		reverseProxy.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:      certPool,
				Certificates: []tls.Certificate{clientCert},
			},
			DialContext: (&net.Dialer{
				Timeout:   ltconst.DialTimeout,
				KeepAlive: ltconst.DialAliveTime,
			}).DialContext,
			TLSHandshakeTimeout:   ltconst.TLSHandshakeTimeout,
			ResponseHeaderTimeout: ltconst.ResponseHeaderTimeout,
			ExpectContinueTimeout: ltconst.ExpectContinueTimeout,
		}
	} else {
		reverseProxy.Transport = &http.Transport{
			DialContext: (&net.Dialer{
				Timeout:   ltconst.DialTimeout,
				KeepAlive: ltconst.DialAliveTime,
			}).DialContext,
			TLSHandshakeTimeout:   ltconst.TLSHandshakeTimeout,
			ResponseHeaderTimeout: ltconst.ResponseHeaderTimeout,
			ExpectContinueTimeout: ltconst.ExpectContinueTimeout,
		}
	}

	return reverseProxy
}

// NewReverseProxyPool create a new reverse proxy pool with the specified number of reverse proxies
func NewReverseProxyPool(backendURL *url.URL, mTLS bool, certPool *x509.CertPool, clientCert tls.Certificate, maxConnections int) *ReverseProxyPool {
	pool := &ReverseProxyPool{}

	for i := 0; i < maxConnections; i++ {
		reverseProxy := NewReverseProxy(backendURL, mTLS, certPool, clientCert)
		pool.reverseProxies = append(pool.reverseProxies, reverseProxy)
	}

	return pool
}

// GetReverseProxy returns a reverse proxy from the pool
func (pool *ReverseProxyPool) GetReverseProxy() *httputil.ReverseProxy {
	pool.mu.Lock()
	defer pool.mu.Unlock()

	if len(pool.reverseProxies) == 0 {
		return nil // No available connections
	}

	reverseProxy := pool.reverseProxies[0]
	pool.reverseProxies = pool.reverseProxies[1:]

	return reverseProxy
}

// ReleaseReverseProxy release a reverse proxy back to the pool
func (pool *ReverseProxyPool) ReleaseReverseProxy(reverseProxy *httputil.ReverseProxy) {
	if reverseProxy == nil {
		return
	}

	pool.mu.Lock()
	defer pool.mu.Unlock()

	pool.reverseProxies = append(pool.reverseProxies, reverseProxy)
}
