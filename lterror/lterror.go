package lterror

type ErrorCode int32

const (
	ErrorCode_ALL_GOOD                ErrorCode = 0
	ErrorCode_BIND_FAILED             ErrorCode = 1
	ErrorCode_VALIDATE_FAILED         ErrorCode = 2
	ErrorCode_JWT_CLAIM_ERROR         ErrorCode = 3
	ErrorCode_JWT_ROLE_ERROR          ErrorCode = 4
	ErrorCode_JWT_AUTHRORIATION_ERROR ErrorCode = 5
	ErrorCode_TRADING_GRPC_ERROR      ErrorCode = 6
	ErrorCode_LOGIN_FAILED            ErrorCode = 10001
	ErrorCode_LOGIN_INVALID_TOKEN     ErrorCode = 10002
	ErrorCode_LOGIN_FIRST_NEEDED      ErrorCode = 10003

	ErrorCode_CAN_NOT_EDIT_GROUP_DEU_TO_OPENING_ORDER ErrorCode = 115005
	ErrorCode_ACCOUNT_INFO_IS_EMPTY                   ErrorCode = 115006
	ErrorCode_GET_TRADER_INFO_FAILED                  ErrorCode = 115008
	ErrorCode_UPDATE_FAILED                           ErrorCode = 115009
)
