package models

type GetSymbolGroupReponse struct {
	AccountNumber string `json:"account_number"`
	GroupID       uint64 `json:"group_id"`
	GroupName     string `json:"group_name"`
}
type UserHasOrder struct {
	AccountNumber string `json:"account_number"`
	HasOrder      int64  `json:"hasorder"`
}
