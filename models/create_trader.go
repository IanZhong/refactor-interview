package models

type AccountSymbolGroupQueryResponse struct {
	AccountNumber   string `json:"account_number"`
	GroupID         int64  `json:"group_id"`
	GroupName       string `json:"group_name"`
	BaseCurrency    string `json:"base_currency"`
	MarginCallLevel string `json:"margin_call_level"`
	StopOutLevel    string `json:"stop_out_level"`
}

type AssembleCreateAccountResponse struct {
	Server        string            `json:"server"`
	AccountNumber string            `json:"account_number"`
	Password      string            `json:"password"`
	Group         GroupListResponse `json:"group"`
}

type GroupListResponse struct {
	ID              string `json:"id"`
	Name            string `json:"name"`
	BaseCurrency    string `json:"base_currency"`
	MarginCallLevel string `json:"margin_call_level"`
	StopOutLevel    string `json:"stop_out_level"`
}
