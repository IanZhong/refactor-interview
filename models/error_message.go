package models

type ErrorResponse struct {
	Message error `json:"message"`
}

type CustomError struct {
	Message   string            `json:"message"`
	ErrorCode int               `json:"error_code"`
	Params    map[string]string `json:"params"`
}
