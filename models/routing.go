package models

type PathInfo struct {
	Target string
	Proxy  string
	MTLS   string
}
