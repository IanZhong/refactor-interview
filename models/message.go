package models

type Message struct {
	Code    int         `json:"code,omitempty"`
	Message interface{} `json:"message,omitempty"`
}
