package models

type ListWithPage struct {
	Data       interface{} `json:"data"`
	Pagination Pagination  `json:"pagination"`
}
type Pagination struct {
	Total string `json:"total"`
	Page  string `json:"page,omitempty" form:"page" query:"page"`
	Limit string `json:"limit,omitempty" form:"limit" query:"limit"`
	Sort  string `json:"sort,omitempty" form:"sort" query:"sort" validate:"oneof='' 'desc' 'asc'"`
	Field string `json:"field,omitempty" form:"field" query:"field"`
}

type TraderList struct {
	UID           string `json:"uid"`
	AccountNumber string `json:"account_number"`
	GroupId       string `json:"group_id"`
	GroupName     string `json:"group_name"`
	Firstname     string `json:"firstname"`
	Lastname      string `json:"lastname"`
	Email         string `json:"email"`
	PhoneNumber   string `json:"phone_number"`
	Currency      string `json:"currency"`
	Country       string `json:"country"`
	Lang          string `json:"lang"`
	Balance       string `json:"balance"`
	Floating      string `json:"floating"`
	CreatedAt     string `json:"created_at"`
	UpdatedAt     string `json:"updated_at"`
	LastVisited   string `json:"last_visited"`
	Server        string `json:"server"`
	Regulation    string `json:"regulation"`
	Status        string `json:"status"`
}

type TraderListWithPage struct {
	Data       []*TraderList   `json:"data"`
	Pagination *PaginationInfo `json:"pagination"`
}

type AccountList struct {
	AccountNumber string `json:"account_number"`
	Balance       string `json:"balance"`
	Floating      string `json:"floating"`
}

type AccountListWithPage struct {
	Data       []*AccountList  `json:"data"`
	Pagination *PaginationInfo `json:"pagination"`
}

type PaginationInfo struct {
	Total string `json:"total"`
	Page  string `json:"page,omitempty" form:"page" query:"page"`
	Limit string `json:"limit,omitempty" form:"limit" query:"limit"`
	Sort  string `json:"sort,omitempty" form:"sort" query:"sort" validate:"oneof='' 'desc' 'asc'"`
	Field string `json:"field,omitempty" form:"field" query:"field"`
}
