package models

type TraderAccountInfo struct {
	AccountNumber int    `json:"account_number"`
	FirstName     string `json:"firstname"`
	LastName      string `json:"lastname"`
	Email         string `json:"email"`
	PhoneNumber   string `json:"phone_number"`
	Currency      string `json:"currency"`
	Country       string `json:"country"`
}
